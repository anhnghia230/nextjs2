import React from "react";
import { HiMenu, HiChatAlt } from "react-icons/hi";

const Header = () => {
  return (
    <div className="w-full h-14 bg-[#fff] flex justify-between px-[20px] items-center border-b-[1px] border-[#d1cece]">
      <div className="flex items-center cursor-pointer">
        <HiMenu className="text-lg" />
        <p className="flex pl-4 text-lg font-bold">Product Dashboard</p>
      </div>
      <div className="flex justify-center items-center w-24 border border-blue-700 p-[4px] rounded-full text-blue-700 cursor-pointer">
        <HiChatAlt className="text-lg" />
        <p className="text-sm pl-2">長方形</p>
      </div>
    </div>
  );
};

export default Header;
