import * as d3 from "d3";
import React from "react";

interface Data {
  date: string;
  number: number;
  percent: number;
}

const data: Data[] = [
  {
    date: "2021-9",
    number: 1120,
    percent: 30,
  },
  {
    date: "2021-10",
    number: 970,
    percent: 33,
  },
  {
    date: "2021-11",
    number: 980,
    percent: 35,
  },
  {
    date: "2021-12",
    number: 1240,
    percent: 37,
  },
  {
    date: "2022-1",
    number: 880,
    percent: 40,
  },
  {
    date: "2022-2",
    number: 1080,
    percent: 43,
  },
  {
    date: "2022-3",
    number: 1470,
    percent: 45,
  },
  {
    date: "2022-4",
    number: 710,
    percent: 47,
  },
  {
    date: "2022-5",
    number: 900,
    percent: 50,
  },
  {
    date: "2022-6",
    number: 1120,
    percent: 53,
  },
  {
    date: "2022-7",
    number: 1100,
    percent: 55,
  },
  {
    date: "2022-8",
    number: 1220,
    percent: 60,
  },
];

const dataline: Data[] = [
  {
    date: "2021-9",
    number: 1120,
    percent: 30,
  },
  {
    date: "2021-10",
    number: 1060,
    percent: 33,
  },
  {
    date: "2021-11",
    number: 1090,
    percent: 35,
  },
  {
    date: "2021-12",
    number: 1100,
    percent: 40,
  },
  {
    date: "2022-1",
    number: 1110,
    percent: 43,
  },
  {
    date: "2022-2",
    number: 1090,
    percent: 45,
  },
  {
    date: "2022-3",
    number: 1270,
    percent: 47,
  },
  {
    date: "2022-4",
    number: 1010,
    percent: 50,
  },
  {
    date: "2022-5",
    number: 1020,
    percent: 53,
  },
  {
    date: "2022-6",
    number: 900,
    percent: 55,
  },
  {
    date: "2022-7",
    number: 980,
    percent: 57,
  },
  {
    date: "2022-8",
    number: 930,
    percent: 60,
  },
];

const dataline2: Data[] = [
  {
    date: "2021-9",
    number: 1120,
    percent: 30,
  },
  {
    date: "2021-10",
    number: 1140,
    percent: 33,
  },
  {
    date: "2021-11",
    number: 1090,
    percent: 35,
  },
  {
    date: "2021-12",
    number: 1060,
    percent: 37,
  },
  {
    date: "2022-1",
    number: 1100,
    percent: 40,
  },
  {
    date: "2022-2",
    number: 1080,
    percent: 43,
  },
  {
    date: "2022-3",
    number: 1180,
    percent: 45,
  },
  {
    date: "2022-4",
    number: 1110,
    percent: 47,
  },

  {
    date: "2022-5",
    number: 1060,
    percent: 50,
  },
  {
    date: "2022-6",
    number: 1090,
    percent: 53,
  },
  {
    date: "2022-7",
    number: 890,
    percent: 55,
  },
  {
    date: "2022-8",
    number: 930,
    percent: 60,
  },
];

const myFunction = () => {
  const margin = { top: 10, right: 60, bottom: 60, left: 60 },
    width = 900 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

  const svg = d3
    .select("#chart")
    .append("svg")
    // .attr("class", "svg1")
    .classed("svg-container", true)
    .attr("preserveAspectRatio", "xMinYMin meet")
    .attr(
      "viewBox",
      `0 0 ${width + margin.left + margin.right} ${
        height + margin.top + margin.bottom
      }`
    )
    // .attr("width", width + margin.left + margin.right)
    // .attr("height", height + margin.top + margin.bottom)
    .style("border-radius", "10px")
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  const xScale = d3
    .scaleBand()
    .domain(
      data.map((d) => {
        return d.date;
      })
    )
    .range([0, width])
    .padding(0.4);

  svg
    .append("g")
    .attr("transform", "translate(0," + height + ")")
    .attr("class", "linex")
    .call(d3.axisBottom(xScale));

  const maxNumber: number = d3.max(data, function (d: Data) {
    return d.number;
  });

  const yScale = d3.scaleLinear().domain([0, maxNumber]).range([height, 0]);
  svg.append("g").call(
    d3
      .axisLeft(yScale)
      .tickFormat((d: number) => d + " " + "本")
      .tickSize(-width)
  );

  const yScaleRight = d3
    .scaleLinear()
    .domain([
      25,
      d3.max(data, function (d: Data) {
        return d.percent;
      }),
    ])
    .range([height, 20.5]);
  svg
    .append("g")
    .attr("transform", "translate(" + width + ",0)")
    .call(
      d3
        .axisRight(yScaleRight)
        .tickFormat((d: number) => d + "%")
        .ticks(7)
    )
    .attr("class", "lineYR");

  const tooltip = d3
    .select("body")
    .append("div")
    .style("position", "absolute")
    .style("border", "1px solid #333")
    .style("border-radius", "10px")
    .style("background", "#f4f4f4")
    .style("padding", "5px 10px");

  const aspect = width / height,
    chart = d3.select("#chart");
  d3.select(window).on("resize", function () {
    const targetWidth = chart.node().getBoundingClientRect().width;
    chart.attr("width", targetWidth);
    chart.attr("height", targetWidth / aspect);
  });

  const colum = svg
    .selectAll("mySta")
    .data(data)
    .enter()
    .append("rect")
    .attr("x", function (d: Data) {
      return xScale(d.date);
    })
    .attr("width", xScale.bandwidth())
    .attr("y", function (d: Data) {
      return height;
    })
    .attr("height", function (d: Data) {
      return 0;
    })
    .attr("fill", "#a7acee")
    .style("opacity", 0.7)
    .on("mouseover", function (this: SVGGraphicsElement, d: any, i: Data) {
      tooltip.transition().style("opacity", 1);
      tooltip
        .html(`<span>${i.date}: ${i.number}</span>`)
        .style("left", d.pageX + "px")
        .style("top", d.pageY + "px");

      d3.select(this).style("opacity", 0.4);
    })
    .on("mouseout", function (this: SVGGraphicsElement) {
      tooltip.transition().style("opacity", 0);
      d3.select(this).style("opacity", 0.7);
    });

  colum
    .transition()
    .attr("y", function (d: Data) {
      return yScale(d.number);
    })
    .attr("height", function (d: Data) {
      return height - yScale(d.number);
    });

  const line = svg
    .append("path")
    .datum(dataline)
    .attr("fill", "none")
    .attr("stroke", "blue")
    .attr("stroke-width", 1)
    .attr("transform", "translate(20,-100)")
    .attr(
      "d",
      d3
        .line()
        .x(function (d: Data) {
          return xScale(d.date);
        })
        .y(function (d: Data) {
          return yScale(d.number);
        })
    );

  const length = line.node().getTotalLength();
  line
    .attr("stroke-dasharray", length + " " + length)
    .attr("stroke-dashoffset", length)
    .attr("stroke-dasharray", 4)
    .transition()
    .ease(d3.easeLinear)
    .attr("stroke-dashoffset", 0)
    .delay(300)
    .duration(2000);

  const line2 = svg
    .append("path")
    .datum(dataline2)
    .attr("fill", "none")
    .attr("stroke", "blue")
    .attr("stroke-width", 1)
    .attr("transform", "translate(20,-100)")
    .attr(
      "d",
      d3
        .line()
        .x(function (d: Data) {
          return xScale(d.date);
        })
        .y(function (d: Data) {
          return yScale(d.number);
        })
    );

  const length2 = line2.node().getTotalLength();
  line2
    .attr("stroke-dasharray", length + " " + length)
    .attr("stroke-dashoffset", length)
    .transition()
    .ease(d3.easeLinear)
    .attr("stroke-dashoffset", 0)
    .delay(300)
    .duration(2000);

  svg
    .append("circle")
    .data(dataline)
    .attr("cx", xScale(dataline[dataline.length - 1]?.date))
    .attr("cy", yScale(dataline[dataline.length - 1]?.number))
    .attr("r", 10)
    .attr("fill", "#616ae0")
    .attr("transform", "translate(20,-100)");

  svg
    .append("circle")
    .data(dataline2)
    .attr("cx", xScale(dataline[dataline2.length - 1]?.date))
    .attr("cy", yScale(dataline[dataline2.length - 1]?.number))
    .attr("r", 10)
    .attr("fill", "#616ae0")
    .attr("transform", "translate(20,-100)");

  svg.selectAll(".domain").style("background", "#fff").style("opacity", 0);

  svg.selectAll(".tick").selectAll("line").style("opacity", 0.2);

  svg
    .selectAll(".linex")
    .selectAll("text")
    .attr("transform", "translate(0,20)");

  svg.selectAll("lineYR").selectAll("text").attr("transform", "translate(0,0)");
};

export default myFunction;
