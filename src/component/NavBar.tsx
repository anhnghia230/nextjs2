import React from "react";

function NavBar() {
  return (
    <div className="w-[240px] min-w-[240px] h-[700px] border-[1px] bg-[#fff] shadow-[10px_0_20px_6px_#e8e8e8] ">
      <div>
        <p className="px-6 pt-10 text-lg cursor-pointer">KGI Dashboard</p>
      </div>
      <div className="px-6 mt-6  text-lg cursor-pointer">
        <p>Product Dashboard</p>
      </div>
    </div>
  );
}

export default NavBar;
