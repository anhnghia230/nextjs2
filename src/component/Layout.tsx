import React from "react";
import Header from "./Header";
import NavBar from "./NavBar";

interface LayoutComponent {
  children: React.ReactNode;
}

const Layout: React.FC<LayoutComponent> = ({ children }) => {
  return (
    <div>
      <Header />
      <div className="flex ">
        <NavBar />
        <div>{children}</div>
      </div>
    </div>
  );
};

export default Layout;
