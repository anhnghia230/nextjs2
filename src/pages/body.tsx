import React from "react";
import HeaderTotal from "./Body/headerTotal";
import HeaderBody from "./Body/headerBody";
import Total from "./Body/total";

const Body: React.FC = () => {
  return (
    <div className=" bg-[#f7f7fc] ml-4">
      <div className="px-[20px] w-[1340px] ">
        <HeaderBody />
      </div>
      <div className="flex">
        <div className="pl-[36px] mt-[14px] font-normal hover:font-black cursor-pointer">
          <span className=" border-b-[2px] hover:border-b-[blue]">た言葉</span>
        </div>
        <div className="pl-[16px] mt-[14px] font-normal hover:font-black cursor-pointer">
          <span className=" border-b-[2px] hover:border-b-[blue]">た言葉</span>
        </div>
      </div>
      <div className="px-[36px]  pt-[4px]">
        <div className="border-[1px]  flex flex-wrap border-t-[transparent] border-r-[transparent] border-l-[transparent]">
          <div className="flex bg-white w-full">
            <HeaderTotal />
          </div>
          <div className="flex-wrap">
            <Total />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Body;
