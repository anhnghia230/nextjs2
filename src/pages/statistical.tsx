import React, { useEffect } from "react";
import d3 from "../component/d3";
import style from "../styles/style.module.css";
import { HiArrowNarrowLeft, HiChartBar, HiTrendingUp } from "react-icons/hi";

const Statistical: React.FC = () => {
  useEffect(() => d3);
  return (
    <div className="px-10 bg-[#f7f7fc] ms:w-[300px]">
      <div className="flex pb-4 items-center">
        <HiArrowNarrowLeft className="text-2xl font-bold" />
        <h1 className="text-2xl font-bold px-2">MPR</h1>

        <p className="text-sm">長い長方形</p>
      </div>
      <div className="lg:flex bg-[#ffffff] p-4 rounded-[10px]  md:flex flex-wrap ">
        <div id="chart" className={style.chart}>
          <div className="flex justify-between md:flex-wrap md:items-center  ">
            <div className="flex">
              <p className="text-xs font-bold pr-1">長い長方形</p>
              <HiChartBar />
            </div>
            <div className="flex mr-2">
              <HiTrendingUp />
              <p className="text-xs font-bold pl-1">長い長方形</p>
            </div>
          </div>
        </div>
        <div className="p-4 ">
          <div className="flex items-center justify-end pb-4">
            <p className="text-xs font-[500] pl-1 pr-2">方形長長</p>
            <select className="border-[1px solid #333] w-[142px] border-[1px]  border-[#333] px-1 text-sm">
              <option value="1" className="text-sm">
                2021.9-2022.8
              </option>
            </select>
          </div>
          <div className="flex items-center justify-end pb-4">
            <p className="text-xs font-[500] pl-1 pr-2">方形</p>
            <select className="border-[1px solid #333] w-[142px] border-[1px]  border-[#333] px-1 text-sm">
              <option value="1" className="text-sm">
                方形長長
              </option>
            </select>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Statistical;
