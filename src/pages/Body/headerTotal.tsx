import React from "react";
import { HiArrowSmDown } from "react-icons/hi";

const HeaderTotal: React.FC = () => {
  return (
    <>
      <div className="text-start w-[180px] pl-[20px] py-[2px] border-[1px] text-[blue] font-semibold border-t-[transparent] border-l-[transparent] ">
        言葉
      </div>
      <div className="w-[130px] flex items-center border-[1px] text-start pl-[4px] font-medium border-t-[transparent]			">
        言葉 <HiArrowSmDown className="text-[#b7bcc0]" />
      </div>
      <div className="flex   border-[1px] border-b-1 border-t-[transparent] border-r-[transparent] ">
        <p className="w-[213px] flex font-medium	 items-center ml-2.5">
          言葉情報を探
          <span className="bg-[#7dcfcc] text-[10px] ml-1 px-[2px] text-[#fff] font-medium	">
            Const
          </span>
          <HiArrowSmDown className="text-[#b7bcc0]" />
        </p>
        <p className="w-[213px] flex font-medium	 items-center pl-[4px]">
          言葉情報を探
          <span className="bg-[#7dcfcc] text-[10px] ml-1 px-[2px] text-[#fff] font-medium	">
            Const
          </span>
          <HiArrowSmDown className="text-[#b7bcc0]" />
        </p>
        <p className="w-[213px] flex font-medium	 items-center pl-[4px]">
          言葉情報を探
          <span className="bg-[#7dcfcc] text-[10px] ml-1 px-[2px] text-[#fff] font-medium	">
            Const
          </span>
          <HiArrowSmDown className="text-[#b7bcc0]" />
        </p>
        <p className="w-[213px] flex font-medium	 items-center pl-[4px]">
          言葉情報を探
          <span className="bg-[#7dcfcc] text-[10px] ml-1 px-[2px] text-[#fff] font-medium	">
            Const
          </span>
          <HiArrowSmDown className="text-[#b7bcc0]" />
        </p>
      </div>
    </>
  );
};

export default HeaderTotal;
