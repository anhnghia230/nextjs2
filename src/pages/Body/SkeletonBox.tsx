import React from "react";
interface Iprop {
  className: string;
}

const SkeletonBox: React.FC<Iprop> = ({ className }) => {
  return (
    <>
      <div className={`animate-pulse ${className}`}></div>
    </>
  );
};

export default SkeletonBox;
