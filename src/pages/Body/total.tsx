import React, { useEffect, useState } from "react";
import SkeletonBox from "./SkeletonBox";

interface wraptotal1text {
  idWrapTotal: number;
  text: string;
  h1: string;
  date: string;
  toalPrice: number;
}

const Wraptotal1: wraptotal1text[] = [
  {
    idWrapTotal: 1,
    text: "さんの言葉",
    h1: "PRECIO",
    date: "2022-10  ",
    toalPrice: 155704,
  },
  {
    idWrapTotal: 2,
    text: "FPD",
    h1: "EXCELINER3300",
    date: "2022-10  ",
    toalPrice: 150112,
  },
  {
    idWrapTotal: 3,
    text: "ます (Batch)",
    h1: "TELINDY",
    date: "2022-10  ",
    toalPrice: 166708,
  },
  {
    idWrapTotal: 4,
    text: "さの言葉",
    h1: "CERTAS",
    date: "2022-10  ",
    toalPrice: 466440,
  },
];

const LoadingTotal = () => {
  return (
    <>
      {Array(4)
        .fill(0)
        .map((item, index) => (
          <div key={index}>
            <div className="flex h-[152px]">
              <div className="  text-start  items-center w-[180px] border-[1px] text-[blue] font-semibold p-[30px] border-l-[transparent] ">
                <SkeletonBox className="w-28 bg-[#d5d8db] h-3" />

                <SkeletonBox className="w-28 bg-[#d5d8db] h-3 mt-1" />

                <div className="flex mt-1">
                  <SkeletonBox className="w-12 bg-[#d5d8db] h-3 " />
                  <SkeletonBox className="w-12 bg-[#d5d8db] h-3 ml-1" />
                </div>

                <div className="flex relative mt-1 ">
                  <SkeletonBox className="w-12 bg-[#d5d8db] h-3 " />
                  <SkeletonBox className="w-12 bg-[#d5d8db] h-3 ml-1" />
                </div>
              </div>
              <div className="w-[130px] flex items-center justify-center border-[1px] 	text-[black] font-bold		">
                <SkeletonBox className="w-28 bg-[#d5d8db] h-3 " />
              </div>
              <div className="flex border-b-[1px] justify-around">
                {Array(5)
                  .fill(0)
                  .map((item, index) => (
                    <div
                      className="flex justify-between items-center pl-4 first:pl-0"
                      key={index}
                    >
                      <SkeletonBox className="w-[180px] h-[126px] ml-[20px] first:ml-3.5 rounded-[20px] bg-[#d5d8db] " />
                    </div>
                  ))}
              </div>
            </div>
          </div>
        ))}
    </>
  );
};

const Total: React.FC = () => {
  const [data, setdata] = useState();

  useEffect(() => {
    setTimeout(() => setdata(Wraptotal1 as any), 3000);
  });

  if (!data) {
    return <LoadingTotal />;
  }

  return (
    <>
      {Wraptotal1.map((item) => (
        <div className="flex" key={item.idWrapTotal}>
          <div className="  text-start  items-center w-[180px] border-[1px] text-[blue] font-semibold p-[30px] border-l-[transparent] ">
            <span className="text-[#c9c6c6]">{item.text}</span>
            <p className="text-[black] text-[16px]">{item.h1}</p>
            <p>
              <span className="text-[10px] bg-[#a4a0a0] text-[#fff] p-[1px]">
                言葉情報
              </span>
              <span className="ml-[10px] text-[10px] bg-[#a4a0a0] text-[#fff] p-[1px]">
                言葉情報
              </span>
            </p>
            <p className="flex relative ">
              <span className="text-[#a4a0a0] text-[12px] pt-[1px]">
                {item.date}
              </span>
              <span className="text-[#a4a0a0] ml-[10px bottom-[20px]] w-[10px] h-[1px] bg-[#a4a0a0] absolute top-[10px] left-[66px]"></span>
            </p>
          </div>
          <div className="w-[130px] flex items-center justify-center border-[1px] 	text-[black] font-bold		">
            {item.toalPrice.toLocaleString("ja-JP", {
              style: "currency",
              currency: "JPY",
            })}
          </div>
          <div className="flex border-b-[1px] justify-around">
            {Array(5)
              .fill(0)
              .map((item, index) => (
                <div
                  className="flex justify-between items-center pl-4 first:pl-0"
                  key={index}
                >
                  <div className="bg-[#fff] w-[180px] h-[126px] ml-2.5 rounded-[20px] flex items-center justify-center ">
                    <p className="h-[1px] w-[150px] bg-[blue]  -rotate-2"></p>
                  </div>
                </div>
              ))}

            <div className="bg-[#fff] w-[180px] h-[126px] ml-[20px] rounded-[20px] flex items-center justify-center ">
              <p className="h-[1px] w-[150px] bg-[blue]  -rotate-2"></p>
            </div>
          </div>
        </div>
      ))}
    </>
  );
};

export default Total;
