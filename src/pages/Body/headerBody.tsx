import React from "react";
import { HiSearch } from "react-icons/hi";

interface content {
  idValue: number;
  optin: string;
  optin1: string;
  optin2: string;
}

const Contents: content[] = [
  {
    idValue: 1,
    optin: "たくさんの言葉",
    optin1: "たくさんの言葉",
    optin2: "たくさんの言葉",
  },
  {
    idValue: 2,
    optin: "たくさんの言葉",
    optin1: "たくさんの言葉",
    optin2: "たくさんの言葉",
  },
  {
    idValue: 3,
    optin: "たくさんの言葉",
    optin1: "たくさんの言葉",
    optin2: "たくさんの言葉",
  },
  {
    idValue: 4,
    optin: "たくさんの言葉",
    optin1: "たくさんの言葉",
    optin2: "たくさんの言葉",
  },
];

const HeaderBody: React.FC = () => {
  return (
    <>
      <div className="pl-[4px] flex justify-between">
        <div className="flex">
          <div className="pt-[16px]  cursor-pointer px-[12px]">
            <input
              type="text"
              placeholder="情報を探す"
              className="border-[1px] p-[6px] w-[140px] h-[26px] border-[#333] outline-none rounded-[4px] "
            />
            <div className="relative">
              <HiSearch className="absolute bottom-[5px] left-[120px] text-[16px]" />
            </div>
          </div>
          {Contents.map((item, index) => (
            <div
              className="pt-[16px] cursor-pointer px-[12px]"
              key={item.idValue}
            >
              <select className="border-[1px]  w-[140px] h-[26px] border-[#333] outline-none rounded-[4px] ">
                <option value={index}>{item.optin}</option>
                <option value={index}>{item.optin1}</option>
                <option value={index}>{item.optin2}</option>
              </select>
            </div>
          ))}
        </div>
        <div>
          <div className="pt-[16px]  cursor-pointer px-[12px] flex">
            <p>情報を探</p>
            <select className="border-[1px] px-[4px] ml-[30px]  w-[220px] h-[26px] border-[#333] outline-none rounded-[4px] ">
              <option value="">{` 6を探 (2022/07-2022/12)`}</option>
            </select>
          </div>
        </div>
      </div>
    </>
  );
};

export default HeaderBody;
